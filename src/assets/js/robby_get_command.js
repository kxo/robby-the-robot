
function getCommands(field, power) {
  var squareSize = Math.sqrt(field.length);

  var position = [];
  var looking = [];

  var commandList = [];

  function getMapPosition(char) {
    var index = field.indexOf(char);
    return [index % squareSize, Math.floor(index / squareSize)];
  }

  function getMapChar(position) {
    var index = position[0] + position[1] * squareSize;
    return field.charAt(index);
  }

  function generateMap() {
    // Notice that positions are (x, y) and matrixes are accessed by [row, col]. x and col represent the same direction
    var map = [];

    // i are row, j are col. position related with cell [i][j] is position (j, i)
    for (var i = 0; i < squareSize; i++) {
      var row = [];
      for (var j = 0; j < squareSize; j++) {
        row[j] = getMapChar([j, i]);
      }
      map.push(row);
    }

    return map;
  }

  var map = generateMap();

  /*
  //***
  function printMatrix(matrix) {
    for (var i = 0; i < matrix.length; i++) {
      console.log('\t' + matrix[i].join('\t'));
    }
    console.log('--');
  }

  function printMins(matrix) {
    var row;
    for (var i = 0; i < matrix.length; i++) {
      row = [];
      for (var j = 0; j < matrix[i].length; j++) {
        row.push(matrix[i][j].min);
      }
      console.log('\t' + row.join('\t'));
    }
    console.log('--');
  }
  //***
 */

  var sPos = getMapPosition("S");
  var tPos = getMapPosition("T");

  var tRow = tPos[1]; // coodrinate y
  var tCol = tPos[0]; // coordinate x

  var sRow = sPos[1]; // coordinate y
  var sCol = sPos[0]; // coordinate x

  /**
   * This function uses the position of T and S and the complete Map to generate an equivalent Map with a value
   * at each cell that can be walked on. The values are generated with the algorithm that simulate every path that
   * reaches the T. The values have the significate of the 'steps needed to reach T from that cell'.
   * It returns:
   *   at [0]: the map with all the values.
   *   at [1]: an object with properties 'going$direction$' (top, right, left, bottom), 'min' and 'bestChoice'
   *     -it has other props, but they are not useful-. 'going$direction$' have the amount of battery needed to reach
   *     the T, going to that specific $direction$. 'min' has the minimum amount of battery required to reach T.
   *     'bestChoice' is an array that stores the all the $direction$'s that lead to the target with the least amount
   *     of battery.
   */
  function generatePointsMap() {
    // needs access to tPos, sPos and map (can be obtained with field and getMapChar(position));

    /**
     * Returs an object with the cells at the positions relative to the cell with 'row = argument row' and 'col = argument col'.
     * The structure is always:
     *   result {top:.., right:.., bottom:.., left:..}
     * If there is no cell at one direction, the value of that property is null.
     */
    function sorroundingCells(row, col) {
      var result = {
        top: [row - 1, col],
        right: [row, col + 1],
        bottom: [row + 1, col],
        left: [row, col - 1]
      };

      // make null all the cells outside the boundaries
      Object.keys(result).forEach(function(direction) {
        var row = result[direction][0];
        var col = result[direction][1];
        if (row < 0 || row == squareSize || col < 0 || col == squareSize) {
          result[direction] = null;
        }
      });

      return result;
    }

    function generatePointsBase() {
      var pointsBase = [];
      var row;
      for (var i = 0; i < map.length; i++) {
        row = [];
        for (var j = 0; j < map[i].length; j++) {
          if (map[i][j] === '.' || map[i][j] === 'S') {
            row.push('');
          } else {
            row.push(map[i][j]);
          }
        }
        pointsBase.push(row);
      }
      return pointsBase;
    }

    function generatePointsByDirection() {
      function PointsObject() {
        this.top = Infinity;
        this.right = Infinity;
        this.bottom = Infinity;
        this.left = Infinity;
        this.min = Infinity
      };
      var matrix = [];
      var row;
      for (var i = 0; i < squareSize; i++){
        row = [];
        for (var j = 0; j < squareSize; j++) {
          row.push(new PointsObject);
        }
        matrix.push(row);
      }

      return matrix;
    }

    var pointsMap = generatePointsBase();
    var pointsByDirection = generatePointsByDirection();

    var leadingCells = [
      [tRow, tCol]
    ];
    var newLeadingCells = [];
    pointsMap[tRow][tCol] = 0;
    pointsByDirection[tRow][tCol] = {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      min: 0
    };

    while (leadingCells.length > 0) {
      newLeadingCells = [];

      leadingCells.forEach(function(leadCell) {
        // for each lead cell, check if its points can be spread to the sorrounding cells
        var sorroundingCellsObject = sorroundingCells(leadCell[0], leadCell[1]);
        Object.keys(sorroundingCellsObject).forEach(function(spreadDirection) {
          // for each cell that was sorrounding the lead one (checkCell), checks if the
          // corresponding value (givingPoints) is the lowest present in that cell.
          // In case it is, writes the new value at both 'min' and the corresponding direction.
          // the corresponding points are the lowest between 'the points in the same direction + 1' or 'the points at min + 2`
          var checkCell = sorroundingCellsObject[spreadDirection];
          // checkCell === null means that checkCell is out of the map.
          if (checkCell !== null && pointsMap[checkCell[0]][checkCell[1]] === '') {

            var sameDirectionPoints = pointsByDirection[leadCell[0]][leadCell[1]][spreadDirection] + 1;
            var turningPoints = pointsByDirection[leadCell[0]][leadCell[1]].min + 2;
            var givingPoints = Math.min(sameDirectionPoints, turningPoints);
            // this is likely to be sameDirectionPoints, but there might be situations in which
            // turningPoints are lower, depending on the distribution of the rocks.

            if (givingPoints <= pointsByDirection[checkCell[0]][checkCell[1]].min) {
              // points are spread
              newLeadingCells.push(checkCell);
              pointsByDirection[checkCell[0]][checkCell[1]][spreadDirection] = givingPoints;
              pointsByDirection[checkCell[0]][checkCell[1]].min = givingPoints;
            }
          }
        });
      });

      leadingCells = newLeadingCells;
    }

    // Give the value of min to each cell that was empty
    for (var i = 0; i < pointsMap.length; i++) {
      for (var j = 0; j < pointsMap[i].length; j++) {
        if (pointsMap[i][j] == '') {
          pointsMap[i][j] = pointsByDirection[i][j].min;
        } else {
          pointsMap[i][j] = Infinity;
        }
      }
    }

    var sPoints = pointsByDirection[sRow][sCol];
    // WATCH OUT! 'top' means that S was at the top of the next cell on the path,
    // and therefore the direction to reach that cell is bottom. So the properties
    // are the direction opposite to the actual direction to take. To make the code
    // more readable, I will use other properties 'going$direction$'
    sPoints.goingTop = sPoints.bottom;
    sPoints.goingRight = sPoints.left;
    sPoints.goingBottom = sPoints.top;
    sPoints.goingLeft = sPoints.right;
    sPoints.min = Infinity;
    sPoints.bestChoice = [];

    // add to sValues the correction for the initial turns
    var turnsBefore = {
      goingTop: 0,
      goingRight: 1,
      goingBottom: 2,
      goingLeft: 1
    };
    ['goingTop', 'goingRight', 'goingBottom', 'goingLeft'].forEach(function(goingDirection) {
      sPoints[goingDirection] += turnsBefore[goingDirection];
      if (sPoints[goingDirection] < sPoints.min) {
        sPoints.min = sPoints[goingDirection];
        sPoints.bestChoice = [goingDirection];
      } else if (sPoints[goingDirection] === sPoints.min) {
        sPoints.bestChoice.push(goingDirection);
      }
    });

    return [pointsMap, sPoints];
  }

  var pointsMapResult = generatePointsMap();
  var pointsMap = pointsMapResult[0];
  var sValues = pointsMapResult[1];

  /*
  //***
  console.log('map matrix');
  printMatrix(map);
  console.log('points map matrix');
  printMatrix(pointsMap);
  //console.log('sValues');
  //console.dir(sValues);
  //***
   */

  /**
   * RULES:
   * 1. Using 'sVAlues', get the 'bestChoice' property, that is the name of the property with the lowest value.
   *   Notice that the name of the property is 'going$Direction$', written with camelCase.
   * 2. For turning, the cell at your side must be STRICTLY SMALLER than the cell in front of you.
   *
   * Basically, at the beginning you choose the best direction (lower is better), and from this point, you just
   * keep walking forward unless a cell at your side is strictly smaller than the cell in front of you
   * in which case you would turn to that side.
   */

  // exit if the best path consume more power than the given power.
  if (sValues.min > power) {
    // target cannot be reached
    return [];
  }

  position = [sRow, sCol];
  looking = [-1, 0]

  function moveForward() {
    position = [position[0] + looking[0], position[1] + looking[1]];
    commandList.push('f');
  }

  function turnLeft() {
    looking = [-looking[1], looking[0]];
    commandList.push('l');
  }

  function turnRight() {
    looking = [looking[1], -looking[0]];
    commandList.push('r');
  }

  function insideBoundaries(pos) {
    return (pos[0] >= 0 && pos[0] < squareSize && pos[1] >= 0 && pos[1] < squareSize);
  }

  function valueAtLeft() {
    var coordinates = [position[0] - looking[1], position[1] + looking[0]];
    if (insideBoundaries(coordinates)) {
      return pointsMap[coordinates[0]][coordinates[1]];
    } else {
      return Infinity;
    }
  }

  function valueAtRight() {
    var coordinates = [position[0] + looking[1], position[1] - looking[0]];
    if (insideBoundaries(coordinates)) {
      return pointsMap[coordinates[0]][coordinates[1]];
    } else {
      return Infinity;
    }
  }

  function valueAtFront() {
    var coordinates = [position[0] + looking[0], position[1] + looking[1]];
    if (insideBoundaries(coordinates)) {
      return pointsMap[coordinates[0]][coordinates[1]];
    } else {
      return Infinity;
    }
  }

  function targetReached() {
    return (position[0] === tRow && position[1] === tCol);
  }

  var atLeft;
  var atRight;
  var atFront;

  // first move
  if (sValues.bestChoice.length > 1) {
    console.log('multiple solutions with the lowest power consumption');
    sValues.bestChoice.forEach(function(goingDirection) {
      console.log('\t- ' + goingDirection + ' power need: ' + sValues[goingDirection]);
    });
    console.log('');
  }

  // if there are multiple solutions, it retuns the first found
  switch (sValues.bestChoice[0]) {
    case 'goingTop':
      // currently oriented top
      break;
    case 'goingRight':
      turnRight();
      break;
    case 'goingLeft':
      turnLeft();
      break;
    case 'goingBottom':
      // here you can turn twice right, or twice left. This would be multiple
      // solutions strictly speaking, but actually the path is the same, so
      // I will just set a preference for turning right and consider both
      // solutions ('rr...' and 'll...') the same solution. This fact should
      // be actually handled at the tests, and not here.
      turnRight();
      turnRight();
      break;
    default:
    // do nothing
  }

  // following moves
  while (!targetReached() && commandList.length < power) {
    atLeft = valueAtLeft();
    atRight = valueAtRight();
    atFront = valueAtFront();

    if (atFront <= atLeft && atFront <= atRight) {
      moveForward();
    } else {
      if (atLeft < atRight) {
        turnLeft();
      } else {
        // has preference for turning right (strictly minor than)
        turnRight();
      }
    }
  }

  return commandList;
}

