import {Component} from '@angular/core';
import {serveWebpackBrowser} from "@angular-devkit/build-angular/src/dev-server";


@Component({
  selector: 'icons-cmp',
  moduleId: module.id,
  templateUrl: 'robby.component.html'
})

export class RobbyComponent {
  /**
   * CONSTANTS
   */
  FIELD_ROBOT = 'S';
  FIELD_EMPTY = '.';
  FIELD_HOME = 'T';
  FIELD_BLOCK = '#';

  /**
   * INPUT
   */
  robby_field_input = 'S.........##.#.#.#.....##.##...#.......##.....#T.';
  power_input = 30;

  /**
   * GRAPH MODELS
   */
  field_array = [["S", ".", ".", ".", ".", ".", "."], [".", ".", ".", "#", "#", ".", "#"], [".", "#", ".", "#", ".", ".", "."], [".", ".", "#", "#", ".", "#", "#"], [".", ".", ".", "#", ".", ".", "."], [".", ".", ".", ".", "#", "#", "."], [".", ".", ".", ".", "#", "T", "."]];
  robby_commands = ["r", "f", "f", "f", "f", "f", "r", "f", "f", "r", "f", "l", "f", "f", "l", "f", "f", "r", "f", "f", "r", "f"];
  direction_arrows = [
    {"x": 0, "y": 0, "rotate": 360}, {"x": 1, "y": 0, "rotate": 360}, {"x": 2, "y": 0, "rotate": 360},
    {"x": 3, "y": 0, "rotate": 360}, {"x": 4, "y": 0, "rotate": 360}, {"x": 5, "y": 0, "rotate": 450}, {"x": 5,"y": 1,"rotate": 450},
    {"x": 5, "y": 2, "rotate": 540}, {"x": 4, "y": 2, "rotate": 450}, {"x": 4, "y": 3, "rotate": 450}, {"x": 4,"y": 4,"rotate": 360},
    {"x": 5, "y": 4, "rotate": 360}, {"x": 6, "y": 4, "rotate": 450}, {"x": 6, "y": 5, "rotate": 450}, {"x": 6,"y": 6,"rotate": 540}
  ];
  robot_coordinates = {x: 0, y: 0};


  onClickMe() {
    this.parseFieldString(this.robby_field_input);
    // @ts-ignore
    this.robby_commands = getCommands(this.robby_field_input, this.power_input);
    this.direction_arrows = [];


    let tmp_arrow = {
      x: this.robot_coordinates.x,
      y: this.robot_coordinates.y,
      rotate: 270
    };
    let self = this;
    this.robby_commands.forEach(function (value, index) {
      if (value == 'r') {
        tmp_arrow.rotate += 90;
      }

      if (value == 'l') {
        tmp_arrow.rotate -= 90;
      }

      if (value == 'f') {
        self.direction_arrows.push(JSON.parse(JSON.stringify(tmp_arrow)));
        switch (tmp_arrow.rotate % 360) {
          case 0:
            tmp_arrow.x++;
            break;
          case 90:
            tmp_arrow.y++;
            break;
          case 180:
            tmp_arrow.x--;
            break;
          case 270:
            tmp_arrow.y--;
            break;
        }
      }

    });
  }

  /**
   * Заполнение:
   * @see field_array
   *
   * @param field_string  строка в формате поля NxN  (Example: "S...#...T")
   * @throws string Ошибка трансформации строки в массив
   */
  parseFieldString(field_string) {
    let length = field_string.length;
    let square_size = Math.sqrt(length);
    if (square_size % 1 != 0) { // Поле обязательно является квадратным
      throw "Некорректная длина строки."
    }

    let robot_index = field_string.indexOf('S');
    this.robot_coordinates.x = robot_index % square_size;
    this.robot_coordinates.y = Math.floor(robot_index / square_size);

    let output_array = [];
    let tmp_string = "";
    for (let i = 0; i < square_size; i++) {
      // Вырезаем кусок строки
      tmp_string = field_string.substring(i * square_size, (i + 1) * square_size);

      // Делим на блоки
      output_array.push(
        tmp_string.split("")
      );
    }

    this.field_array = output_array;
  }


}
