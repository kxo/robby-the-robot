import { Routes } from '@angular/router';

import {RobbyComponent} from "../../pages/robby/robby.component";

export const AdminLayoutRoutes: Routes = [
    { path: 'robby',          component: RobbyComponent }
];
